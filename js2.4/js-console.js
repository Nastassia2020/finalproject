﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function check(){
	let arr = document.querySelector('#inp').value.split(' ');
	let arrCheck1 = [];
	let arrCheck2 = [];
	for (i = 1; i < arr.length; i++) {
        if (arr[i] > arr[i-1]) {
            arrCheck2.push(arr[i]);
        } else {
            if (arrCheck2.length > arrCheck1.length) {
                arrCheck1 = arrCheck2;
            }
            arrCheck2 = [arr[i]];
        }
    }
    if (arrCheck2.length > arrCheck1.length) {
        arrCheck1 = arrCheck2;
	}
	jsConsole.write('The input sequence is: ' + arr + '<br>');
	jsConsole.write('The maximum Increasing sequence is: ' + arrCheck1);
}

document.querySelector('btn').addEventListener('click', check);
