function caseChange(){
    let str = document.querySelector('.str').value;

    jsConsole.write(str.replace(/<upcase>([\s\S]*?)<\/upcase>/g,
    (m, g1) => g1.toUpperCase()).replace(/<lowcase>([\s\S]*?)<\/lowcase>/g,
    (m, g1) => g1.toLowerCase()).replace(/<mixcase>([\s\S]*?)<\/mixcase>/g,
    (m, g1) => [...g1].map(e => Math.floor(Math.random() * 2) ? e.toLowerCase() : e.toUpperCase()).join``));
}