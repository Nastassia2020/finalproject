function checkString()
    {
        let str = document.querySelector('.str').value;
        let stack = [];

        for(let i = 0; i < str.length; i++){
            if(str[i] === '(' && str[i+1] === ')'){
                jsConsole.write('Invalid Expression');
                return
            }
            if(str[i] === '('){
                stack.push(str[i]);
            }
            if(str[i] === ')'){
                if(!stack.length){
                    jsConsole.write('Invalid Expression');
                    return
                }
                stack.pop();
            }
        }
        if(stack.length) {
            jsConsole.write('Invalid Expression');
            return
        }
        jsConsole.write('Valid Expression');
    }