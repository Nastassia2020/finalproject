let x1 = 3;
let y1 = 3;
let x2 = 5;
let y2 = 5;
let x3 = 2;
let y3 = 2;
let x4 = 4;
let y4 = 4;
let x5 = 1;
let y5 = 1;
let x6 = 3;
let y6 = 3;

let createPoint = function(x, y){
    return p = {x: x, y: y};
}

let p1 = createPoint(x1, y1);
let p2 = createPoint(x2, y2);
let p3 = createPoint(x3, y3);
let p4 = createPoint(x4, y4);
let p5 = createPoint(x5, y5);
let p6 = createPoint(x6, y6);

let createLine = function(o1, o2){
    return l = {o1: Object.values(o1), o2: Object.values(o2)};
}

let l1 = createLine(p1, p2);
let l2 = createLine(p3, p4);
let l3 = createLine(p5, p6);

let calcDistance = function(l){
    let arr = Object.values(l);
    return Math.sqrt((arr[1][0] - arr[0][0])**2 + (arr[1][1] - arr[0][1])**2);
}

let a = calcDistance(l1);
let b = calcDistance(l2);
let c = calcDistance(l3);

function canBeTriangle(){
    if((a+b)>c && (a+c)>b && (c+b)>a){
        jsConsole.write('The lines CAN create triangle' + '<br>' + '<br>'
        + 'Line 1:' + '<br>'
        + 'Line point A(' + x1 + ', ' + y1 + ')' + '<br>'
        + 'Line point B(' + x2 + ', ' + y2 + ')' + '<br>'
        + 'Line 2:' + '<br>'
        + 'Line point A(' + x3 + ', ' + y3 + ')' + '<br>'
        + 'Line point B(' + x4 + ', ' + y4 + ')' + '<br>'
        + 'Line 3:' + '<br>'
        + 'Line point A(' + x5 + ', ' + y5 + ')' + '<br>'
        + 'Line point B(' + x6 + ', ' + y6 + ')' + '<br>');
    }
}

let x7 = 3;
let y7 = 3;
let x8 = 15;
let y8 = 15;
let x9 = 5;
let y9 = 2;
let x10 = 4;
let y10 = 1;
let x11 = 1;
let y11 = 1;
let x12 = 3;
let y12 = 2;

let createPoint2 = function(x, y){
    return p = {x: x, y: y};
}

let p7 = createPoint2(x7, y7);
let p8 = createPoint2(x8, y8);
let p9 = createPoint2(x9, y9);
let p10 = createPoint2(x10, y10);
let p11 = createPoint2(x11, y11);
let p12 = createPoint2(x12, y12);

let createLine2 = function(o1, o2){
    return l = {o1: Object.values(o1), o2: Object.values(o2)};
}

let l4 = createLine2(p7, p8);
let l5 = createLine2(p9, p10);
let l6 = createLine2(p11, p12);

let calcDistance2 = function(l){
    let arr = Object.values(l);
    return Math.sqrt((arr[1][0] - arr[0][0])**2 + (arr[1][1] - arr[0][1])**2);
}

let d = calcDistance2(l4);
let e = calcDistance2(l5);
let f = calcDistance2(l6);

function notTriangle(){
    if((d+e)>f && (d+f)>e && (f+e)>d){
        jsConsole.write('true');
    }else{
        jsConsole.write('The lines CANNOT create triangle' + '<br>' + '<br>'
        + 'Line 1:' + '<br>'
        + 'Line point A(' + x7 + ', ' + y7 + ')' + '<br>'
        + 'Line point B(' + x8 + ', ' + y8 + ')' + '<br>'
        + 'Line 2:' + '<br>'
        + 'Line point A(' + x9 + ', ' + y9 + ')' + '<br>'
        + 'Line point B(' + x10 + ', ' + y10 + ')' + '<br>'
        + 'Line 3:' + '<br>'
        + 'Line point A(' + x11 + ', ' + y11 + ')' + '<br>'
        + 'Line point B(' + x12 + ', ' + y12 + ')' + '<br>');
    }
}
