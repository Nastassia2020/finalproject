let fib = function(n){
    let a = 1;
    let b = 1;
    for (let i = 3; i <= n; i++) {
      let c = a + b;
      a = b;
      b = c;
    }
    return b;
}

jsConsole.write(fib(3) + '</br>');
jsConsole.write(fib(7) + '</br>');
jsConsole.write(fib(77) + '</br>');