﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function sort(){
	let str = document.querySelector('.arrays').value.split(' ');
	let arr = str.map(item => parseInt(item));
	let len = arr.length;
    for (let i = 0; i < len-1; i++)
    {let min = i;
       for (let j = i+1; j < len; j++)
        { if (arr[j] < arr[min]) min = j; }
	   let n = arr[min];
	   arr[min] = arr[i];
	   arr[i] = n;
    }
	jsConsole.write('The sorted array is: ' + arr);
}

document.querySelector('btn').addEventListener('click', check);
