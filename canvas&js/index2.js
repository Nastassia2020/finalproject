(function dragDrop(){
    const dragEl = document.querySelectorAll('.logo');
    const dropZone = document.querySelector('.trash_close');
    let coordX;
    let coordY;
    let trash = '';

    dragEl.forEach(element => {
        element.addEventListener('dragstart', (e) => {
            e.dataTransfer.setData('text/html', 'dragstart');
            coordX = e.offsetX;
            coordY = e.offsetY;
            trash = e.currentTarget;
        });
        element.addEventListener('dragend', (e) => {
            element.style.position = 'absolute';
            element.style.top = (e.pageY - coordY) + 'px';
            element.style.left = (e.pageX - coordX) + 'px';
        });
    })

    document.addEventListener("dragover", (e) => {
        e.preventDefault();
    }, false);

    dropZone.addEventListener('dragenter', (e) => {
        if ( e.target.className == "trash_close" ) {
            e.target.className = "trash_open";
        }
    });

    dropZone.addEventListener('dragleave', (e) => {
        if ( e.target.className == "trash_open" ) {
            e.target.className = "trash_close";
        }
    });

    dropZone.addEventListener("drop", (e) => {
        e.preventDefault();
        if ( dropZone ) {
            e.target.style.background = "";
            trash.remove();
        }
        dropZone.classList.remove("trash_open");
        dropZone.classList.add("trash_close");
    }, false);
})();



