function findWith(){
    let arr = document.querySelector('.txt').value.split(' ');
	let num = document.querySelector('.word').value;
	let max = 0;
	let count = 0;
    for (let i = 0; i < arr.length; i++) {
		if(arr[i] == num){
			count++;
		} else if (count > max){
			max = count;
		}
    }
    jsConsole.write('There are ' + count + ' occurrences of the word ' + '"' + num + '"');
}

function findWithout(){
    let str = document.querySelector('.txt').value.toLowerCase();
    let arr = str.split(' ');
    let num = document.querySelector('.word').value.toLowerCase();
	let max = 0;
	let count = 0;
    for (let i = 0; i < arr.length; i++) {
		if(arr[i] == num){
			count++;
		} else if (count > max){
			max = count;
		}
    }
    jsConsole.write('There are ' + count + ' occurrences of the word ' + '"' + num + '"');
}

function ev(){
    let check1 = document.querySelector(".checkbox");
    if(check1.checked){
       return findWith();
    } else {
        return findWithout();
    }
}
document.querySelector(".btn").addEventListener("click", ev)