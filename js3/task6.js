function check(){
    let str = document.querySelector('.array').value.split(' ');
    let arr = str.map(item => parseInt(item));
    let num = parseInt(document.querySelector('.search').value);

    if(arr[num] > arr[num + 1] && arr[num] > arr[num - 1] && num > 0){
        jsConsole.write('true' + '<br>');
    } else if(num === 0 || num === (-1)){
        jsConsole.write('The position cannot be the first or the last element in the sequence!');
    } else {
        jsConsole.write('false' + '<br>');
    }
}