function palindrome(){
    let str = document.querySelector('.arrays').value;
    let arr = str.split(' ');
    let palindrome = [];

    for(let i = 0; i < arr.length; i++){
        let strReverse = arr[i].split('').reverse().join('');
        if (strReverse === arr[i]) {
            palindrome.push(arr[i]);
        }
    }

    jsConsole.write(palindrome);
}