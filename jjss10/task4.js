function makeList(){
    let people = [
        { name: "Шапокляк", age: 55 },
        { name: "Чебурашка", age: 17 },
        { name: "Крыска-Лариска", age: 18 },
        { name: "Крокодильчик", age: 26 },
        { name: "Турист- завтрак крокодильчика", age: 32 },
    ]

    let divPeople = document.getElementById("list-item");

    function generateHtmlList(arr){
        let ul = document.createElement('ul');
        divPeople.appendChild(ul);
        let str = '<strong>-{name}-</strong> <span>-{age}-</span>';

        for(let i = 0; i < arr.length; i++){
            let li = document.createElement('li');
            let n = str.replace(new RegExp("-{name}-",'g'), arr[i].name);
            let a = n.replace(new RegExp("-{age}-",'g'), arr[i].age);
            li.innerHTML = a;
            ul.appendChild(li);
        }

    }

    generateHtmlList(people);
}
