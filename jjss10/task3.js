function stringFormat(){
    String.prototype.f = function(){
        let args = arguments;
        return this.replace(/\{(\d+)\}/g, function(m,n){
            return args[n] ? args[n] : m;
        });
    };

    let str = "The coordinates of the {0} are X:{1} and Y: {2}".f("rectangle", 25, 36);
    jsConsole.write(str);
}