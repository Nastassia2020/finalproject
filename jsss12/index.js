const main = document.querySelector('#root');
const ul = document.createElement('ul');
const inp = document.createElement('input');
ul.innerText = `Don't forget to:`;
ul.className = 'list';
inp.setAttribute('type', 'text');
inp.setAttribute('placeholder', 'New item here...');
main.append(ul, inp);

function createButton(name){
    const btn = document.createElement('button');
    btn.textContent = name;
    return btn;
}

function addNewElement(){
    const check = `<li><input type='checkbox'> ${inp.value} </li>`;
    ul.style.listStyleType = 'none';
    ul.innerHTML += check;
}

function removeElement(){
    const check = document.querySelectorAll(`input[type='checkbox']`);
    const li = document.querySelectorAll('li');

    check.forEach((item, index) => {
        if(item.checked){
            li[index].remove();
        }
    })
}

function hideElement(){
    const check = document.querySelectorAll(`input[type='checkbox']`);
    const li = document.querySelectorAll('li');
    check.forEach((item, index) => {
        if(item.checked){
            li[index].style.display = 'none';
        }
    })
}

function showElement(){
    const li = document.querySelectorAll('li');
    li.forEach((item, index) => {
        if(item.style.display = 'none'){
            li[index].style.display = 'block';
        }
    })
}

const addNew = createButton('add');
const removeMe = createButton('Remove selected');
const hideMe = createButton('Hide selected');
const showMe = createButton('Show hidden');
main.append(addNew);
main.append(removeMe);
main.append(hideMe);
main.append(showMe);

addNew.addEventListener('click', addNewElement);
removeMe.addEventListener('click', removeElement);
hideMe.addEventListener('click', hideElement);
showMe.addEventListener('click', showElement);