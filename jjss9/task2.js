function url(){
    const text = document.querySelector('.arrays').value;
    const url = new URL(text);
    const obj = {
        protocol: url.protocol,
        server: url.host,
        resource: url.pathname,
    }

    jsConsole.writeLine(JSON.stringify(obj));
}