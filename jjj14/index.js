const root = document.querySelector('#root');



if(!localStorage.getItem('gamers')){
    let gamers = JSON.stringify([]);
    localStorage.setItem('gamers', gamers);
}

function random(){
     const array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
     let randomNum;

        for (let i = array.length - 1; i > 0; i--) {
          let j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
        }
        if(array[0] === 0){
            randomNum = array.slice(1, 4).join('');
        } else{
            randomNum = array.slice(0, 4).join('');
        }
        console.log(randomNum);
    return randomNum
}

function randomNumber(){
    if(!sessionStorage.getItem('randomNum')){
        let num = random();
        console.log(1);
        sessionStorage.setItem('randomNum', num);
    }
    return sessionStorage.getItem('randomNum').split('');
}


let counter = 0;

const win = function(){
    sessionStorage.removeItem('randomNum');
    sessionStorage.setItem('move', counter);
    document.querySelector('#info').style.display = 'none';
    document.querySelector('.hidden').style.display = 'block';
    document.querySelector('#btn2').addEventListener('click', () => {
        let personName = document.querySelector('#inp2').value;
        let move = sessionStorage.getItem('move');
        const person = {name: personName, move: move};
        let gamers = JSON.parse(localStorage.getItem('gamers'));
        gamers.push(person);
        gamers = gamers.sort((a, b) => {a.move - b.move});
        localStorage.setItem('gamers', JSON.stringify(gamers));
    })
    document.querySelector('#result').addEventListener('click', () => {
        console.log(JSON.parse(localStorage.getItem('gamers')))
        let result = JSON.parse(localStorage.getItem('gamers'));
        let ol = document.querySelector('#winners');
        ol.innerHTML = '';
        document.querySelector('#epam').style.display = 'block';
        result.forEach((item) => {
            ol.innerHTML += `<li>${item.name}: ${item.move} moves</li>`
        })
    })
    counter = 0;
}

function game(){
    let err = document.createElement('p');
    err.className = 'err';
    let foo = document.querySelector('#inp1').value.split('');
    let arr = randomNumber();
    let info1 = document.querySelector('#info');
    console.log(arr);
    const unique = Array.from(new Set(foo));

    let sheep = 0;
    let ram = 0;

    if(foo.length != 4 || foo[0] == 0 || unique.length < 4){
        err.innerText = 'Invalid entered number. Number must contain four different digits and cannot start with 0.'
        root.append(err);
    }else {
            foo.map((element, index) => {
                if(element == arr[index]){
                    ram++;
                    return
                } else if(arr.includes(element)){
                    sheep++;
                }
            })

            info1.innerHTML = `You have: ${ram} ram(s) and ${sheep} sheep(s)`;
         if(ram === 4) {
            win();
        }
    }

}


document.querySelector('#btn1').addEventListener('click', () => {
    counter++;
});
document.querySelector('#btn1').addEventListener('click', () => {
    document.querySelector('.hidden').style.display = 'none';
    document.querySelector('#info').style.display = 'block';
    document.querySelector('.err').style.display = 'none';
});
document.querySelector('#btn1').addEventListener('click', game);


