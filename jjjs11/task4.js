let circleArray = [];

function createDivs(){
  let theta = [];
  let n = parseInt(document.querySelector('#num').value);
  let r = parseInt(document.querySelector('#rad').value);
  const setup = function(n, r){
    const main = document.getElementById('main');
    let mainH = 600;
    for (let i = 0; i < n; i++) {
      let circle = document.createElement('div');
      circle.className = 'circle number' + i;
      circleArray.push(circle);
      circleArray[i].posx = Math.round(r * (Math.cos(theta[i]))) + 'px';
      circleArray[i].posy = Math.round(r * (Math.sin(theta[i]))) + 'px';
      circleArray[i].style.position = 'absolute';
      circleArray[i].style.background = 'pink';
      circleArray[i].style.top = ((mainH / 2) - parseInt(circleArray[i].posy.slice(0, -2))) + 'px';
      circleArray[i].style.left = ((mainH / 2) + parseInt(circleArray[i].posx.slice(0, -2))) + 'px';
      main.appendChild(circleArray[i]);
    }
  }

  const generate = function(n, r) {
    let frags = 360 / n;
    for (let i = 0; i <= n; i++) {
      theta.push((frags / 180) * i * Math.PI);
    }
    setup(n, r)
  }

  generate(n, r);
}

console.log(circleArray);

function animateDivs(){
  const colors = ['red', 'green', 'purple', 'orange', 'yellow', 'maroon', 'grey', 'lightblue', 'tomato', 'pink', 'maroon', 'cyan', 'magenta', 'blue', 'chocolate', 'DarkSlateBlue'];
 for(let i = 0; i < circleArray.length; i++){
      const obj = {
      r: parseInt(document.querySelector('#rad').value),
      s: parseInt(document.querySelector('#time').value),
    }
    var f = 0;
    var s = 2 * Math.PI / 40;
    setInterval(function() {
      f += s;
      circleArray[i].style.left =  300 + obj.r * Math.sin(f)  + 'px';
      circleArray[i].style.top =   300 + obj.r * Math.cos(f) + 'px';
    }, obj.s);
    setInterval (function (){
      let color = colors.shift();
      circleArray[i].style.background = color;
      colors.push (color);
    }, obj.s);
 }
}
