
const main = document.querySelector('body');
const lonelyDiv1 = document.createElement('div');
const divWithChildren = document.createElement('div');
const firstChild = document.createElement('div');
const secondChild = document.createElement('div');
const lonelyDiv2 = document.createElement('div');

lonelyDiv1.className = 'lonelyDiv';
divWithChildren.className = 'divWithChildren';
firstChild.className = 'child';
secondChild.className = 'child';
lonelyDiv2.className = 'lonelyDiv';
lonelyDiv1.innerText = 'Lonely Div';
firstChild.innerText = 'Div inside another Div 1';
secondChild.innerText = 'Div inside another Div 2';
lonelyDiv2.innerText = 'Another lonely Div';

main.append(lonelyDiv1);
divWithChildren.appendChild(firstChild);
divWithChildren.appendChild(secondChild);
main.append(divWithChildren, lonelyDiv2);

function withTagName(){
    let div = document.querySelector('.divWithChildren');
    let children = div.getElementsByTagName('div');

    if(children.length != 0){
        for(let i = 0; i < children.length; i++){
            children[i].style.borderColor = 'red';
        }
    }
}

function selector(){
    let div = document.querySelector('.divWithChildren');
    let children = div.childNodes;
    if(children.length != 0){
        for(let i = 0; i < children.length; i++){
            children[i].style.borderColor = 'green';
        }
    }
}