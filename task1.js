function newPerson(){
  let firstName = document.querySelector('.arrays1').value;
  let lastName = document.querySelector('.arrays2').value
  let age = Number(document.querySelector('.arrays3').value);

  if(firstName.length > 20 || firstName.length < 3 || !isNaN(firstName) || !isNaN(lastName)){
    jsConsole.write ('Incorrect Name');
  }else if(age < 0 || age > 150 || isNaN(age)){
    jsConsole.write ('Incorrect Age');
  }else
    {function Person() {
      this.firstName = '';
      this.lastName = '';
      this.age = null;
      this.fullName = '';
    }

    Person.prototype.getFirstName = function(){
      return this.firstName;
    }

    Person.prototype.setFirstName = function(value){
      this.firstName = value;
    }

    Person.prototype.getLastName = function(){
      return this.lastName;
    }

    Person.prototype.setLastName = function(value){
      this.lastName = value;
    }

    Person.prototype.getAge = function(){
      return this.age;
    }

    Person.prototype.setAge = function(value){
      this.age = value;
    }

    Person.prototype.getFullName = function(){
      return this.fullName;
    }

    Person.prototype.setFullName = function(value) {
      this.firstName = value.split(' ')[0];
      this.lastName = value.split(' ')[1];
      this.fullName = `${this.firstName} ${this.lastName}`;
    }

    Person.prototype.introduce = function(){
      return `Hello! My name is ${this.fullName} and I am ${this.age} years-old`;
    }

    let people = new Person();

    people.setFirstName(firstName);
    people.setLastName(lastName);
    people.setAge(age);

    let info = ''.concat(people.getFirstName(), ' ',people.getLastName());
    people.setFullName(info);

    let hello = people.introduce();
    jsConsole.write(hello);
  }
}

