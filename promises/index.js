function give(){

    return new Promise((resolve) => {
        xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = function(){
            if(xmlHttp.readyState === 4 && xmlHttp.status === 200){
                document.querySelector('.forGetRequest').innerHTML = xmlHttp.responseText;
            }
        }

        xmlHttp.open('GET', '01.data.json', true);

        xmlHttp.send();

        document.querySelector('.inp').style.display = 'block';

        resolve();
    })
}

function post(){

    return new Promise((resolve) => {

        xmlHttp = new XMLHttpRequest();

        xmlHttp.open('POST', 'http://test-1.ru/back.php', true);

        xmlHttp.setRequestHeader('Content-type', 'application/json');

        xmlHttp.onreadystatechange = function(){
            if(xmlHttp.readyState === 4 && xmlHttp.status === 200){
                document.querySelector('.forPostRequest').innerHTML = this.responseText;
            }
        }

        let data = JSON.stringify({ "name": firstName.value, "lastname": lastName.value, "age": age.value});

        xmlHttp.send(data);

        console.log(data);
        document.querySelector('.inp').style.display = 'none';

        resolve();
    })
}

document.querySelector('.post').addEventListener('click', post);
document.querySelector('.get').addEventListener('click', give);
//http://test-1.ru/back.php