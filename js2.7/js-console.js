﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		return self;
	}
	jsConsole = new createJsConsole("#console");
}).call(this);

function findIndex(){
	let arr = document.querySelector('.arrays').value.split(' ');
	let t = document.querySelector('.value').value;
	arr.sort(function (a, b) {
		return a - b;
	});
	let i = 0;
	let j = arr.length;
	let k;
	while (i < j)
    {  k = Math.floor((i+j)/2);
       if (t <= arr[k]) j = k;
       else i = k+1;
    }
    if (arr[i] === t) jsConsole.write('Sorted array: ' + '<br>' + arr + '<br>' + 'The index of ' + t + ' is ' + i);
    else return -1;

}
